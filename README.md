# Praktikum Sistem Operasi Modul 1 - Kelompok IT04

## Nomor 1
**SOAL NOMOR 1**

Farrel ingin membuat sebuah playlist yang sangat edgy di tahun ini. Farrel ingin playlistnya banyak disukai oleh orang-orang lain. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa data untuk membuat playlist terbaik di dunia. Untung saja Farrel menemukan file playlist.csv yang berisi top lagu pada spotify beserta genrenya.

1a. Farrel ingin memasukan lagu yang bagus dengan genre hip hop. Oleh karena itu, Farrel perlu melakukan testi terlebih dahulu. Tampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity.

**Pengerjaan 1a:**

`grep "hip hop" playlist.csv > hiphop.csv` 

mengambil file bergenre hiphop dan memasukkannya ke file baru bernama hiphop.csv

`sort -t “,” -k 15,15nr hiphop.csv > sorted_hiphop.csv` 

menyortir di kolom 15 (popularity berada di kolom 15), nr untuk mengurutkan dari nilai terbesar ke terkecil (karena lagu yang paling populer berarti nilai popularitynya besar), lalu dimasukkan ke file baru bernama sorted_hiphop.csv

`head -n 5 sorted_hiphop.csv` 

mengambil 5 nilai teratas di file yang sudah disortir berdasarkan urutan popularity. Karena diurutkan dari nilai terbesar ke terkecil, maka 5 nilai teratasnya adalah lagu terpopuler.

**Output 1a:**

![Output 1a](Gambar/1a.jpg)


1b. Karena Farrel merasa kurang dengan lagu tadi, dia ingin mencari 5 lagu yang paling rendah diantara lagu ciptaan John Mayer berdasarkan popularity.

**Pengerjaan 1b:**

`grep “John Mayer” playlist.csv > johnmayer.csv`

mengambil file dengan nama artis John Mayer dan memasukkannya ke file baru bernama johnmayer.csv

`sort -t “,” -k 15,15 johnmayer,csv > sorted_johnmayer.csv` 

menyortir di kolom 15 (popularity berada di kolom 15), karena lagu yang paling tidak populer berarti nilai popularitynya kecil, jadi tidak memakai nr, lalu dimasukkan ke file baru bernama sorted_johnmayer.csv

`head -n 5 sorted_johnmayer,csv` 

mengambil 5 nilai teratas di file yang sudah disortir berdasarkan urutan popularity. Karena diurutkan dari nilai terkecil, jadi nilai teratasnya adalah lagu yang paling tidak populer dari John Mayer.

**Output 1b:**

![Output 1b](Gambar/1b.jpg)

1c. Karena Farrel takut lagunya kurang enak didengar dia ingin mencari lagu lagi, cari 10 lagu pada tahun 2004 dengan rank popularity tertinggi

**Pengerjaan 1c:**

`grep “2004” playlist.csv > 2004.csv` 

mengambil file yang dirilis di tahun 2004 dan memasukkannya ke file baru bernama 2004.csv

`sort -t “,” -k 15,15nr 2004.csv > sorted_2004.csv` 

menyortir di kolom 15 (popularity berada di kolom 15), nr untuk mengurutkan dari nilai terbesar ke terkecil (karena lagu yang paling populer berarti nilai popularitynya besar), lalu dimasukkan ke file baru bernama sorted_2004.csv

`head -n 10 sorted_2004.csv` 

mengambil 10 nilai teratas di file yang sudah disortir berdasarkan urutan popularity. Karena diurutkan dari nilai terbesar ke terkecil, maka 10 nilai teratasnya adalah lagu terpopuler.

**Output 1c:**

![Output 1c](Gambar/1c.jpg)

1d. Farrel sangat suka dengan lagu paling keren dan edgy di dunia. Lagu tersebut diciptakan oleh ibu Sri. Bantu Farrel mencari lagu tersebut dengan kata kunci nama pencipta lagu tersebut.

**Pengerjaan 1d:**

`grep “Sri” playlist.csv` 

mengambil file yang dengan nama artis ibu Sri. 

**Output 1d:**

![Output 1d](Gambar/1d.jpg)

> Kendala di nomor 1:
di nomor 1b dan 1d, sortnya tidak terbaca di file playlist_keren.sh sehingga tidak ada outputnya, tetapi jika dijalankan dengan command write, bisa muncul outputnya. Tertulis "no such file or directory", tetapi setelah dicek, ada filenya.

> Revisi di nomor 1:
pada file playlist_keren.sh, sebelumnya untuk pengerjaan 1b dan 1c tidak bisa dibaca karena lupa menambahkan "-k" di bagian `sort -t “,” -k 15,15 johnmayer,csv > sorted_johnmayer.csv`



## Nomor 2
Shinichi merupakan seorang detektif SMA yang kembali menjadi anak kecil karena ulah organisasi hitam. Dengan tubuhnya yang mengecil, Shinichi tidak dapat menggunakan identitas lamanya sehingga harus membuat identitas baru. Selain itu, ia juga harus membuat akun baru dengan identitas nya saat ini. Bantu Shinichi membuat program register dan login agar Shinichi dapat dengan mudah membuat semua akun baru yang ia butuhkan.

**Pengerjaan 2a:**

Shinichi akan melakukan register menggunakan email, username, serta password. Username yang dibuat bebas, namun email bersifat unique.

	echo -e  "Name "
	read Name
	echo -e  "Email"
	read email
	echo -e  "Password"
	read password
*Echo untuk mengorientasikan pengguna variabel yang dimasukkan*

	if [ -z "$Name" ]; then
        echo -e "Insert a name"
		F=1
	fi
	if [ -z "$email" ]; then
	        echo -e "Insert an email"
	        F=1
	fi
	if [[ $email =~ ['!@#$%^&*()-_+=|\:;<,>.?/'] ]]; then
	        C=0
	else
		echo "Proper email address required"
		F=1
	fi
*Digunakan untuk mengecek apakah variabel kosong atau tidak*
*Pada line 82 ini untuk mengecek alamat email*
*Variabel F digunakan untuk mencari apakah gagal dalam suatu tahap*

![2a](Gambar/2a.png)


**Pengerjaan 2b:**

Shinichi khawatir suatu saat nanti Ran akan curiga padanya dan dapat mengetahui password yang ia buat. Maka dari itu, Shinichi ingin membuat password dengan tingkat keamanan yang tinggi.
Password tersebut harus di encrypt menggunakan base64
Password yang dibuat harus lebih dari 8 karakter
Harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil
Password tidak boleh sama dengan username
Harus terdapat paling sedikit 1 angka 
Harus terdapat paling sedikit 1 simbol unik 

	if [ $n  -lt $b ];
	then
	echo -e "Less than 8 \n"
	fi
*Mengecek apakah lebih atau sama dari 8*

	if [[ $C =~  [A-Z] ]]; then
	    echo -e "No capital word \n"
	    F=1
	fi
*Mengecek adanya huruf kapital*

	if [[ $nC =~ [a-z] ]]; then
		echo -e "No non-capital   word \n"
		F=1
	fi
*Mengecek adanya huruf non kapital*

	if [[ "$password" == "$Name" ]]; then
		echo -e "Passsword and name is the same \n"
		F=1
	fi
*Mengecek apakah sama dengan username passwordnya*

	if [[ $password =~ [0-9] ]]; then
		N=1
	else
		echo -e "No number \n"
		F=1
	fi
*Mengecek adanya nomor dalam password*

	if [[ $password =~ ['!@#$%^&*()-_+=|\:;<,>.?/'] ]]; then
		UE=1
	else
		echo -e "No unique symbol"
		F=1
	fi
*Mengecek simbol spesial*

*Variabel F digunakan untuk mencari apakah gagal dalam suatu tahap*

	if [ $F -eq  0 ]; then
		echo   "This password is good "
		echo  "Encrypting password"
		encrypted_var=$(echo -n "$password" | openssl enc -e -aes-256-cbc -a -salt -pass pass:rizki4132)

*Jika tidak gagal password akan disimpan dan diencrypt(tidak ada bagian mengecek enkripsi karena suatu hal bisa dienkripsi dan dienkripsi lagi.Oleh karena itu,kita menggunakan kalimat ini sebagai alternatif)*

![2b](Gambar/2b2.png)

**Pengerjaan 2c:**

Karena Shinichi akan membuat banyak akun baru, ia berniat untuk menyimpan seluruh data register yang ia lakukan ke dalam folder users file users.txt. Di dalam file tersebut, terdapat catatan seluruh email, username, dan password yang telah ia buat.

	echo "$Name" >> users.txt
	echo "$email" >> users.txt
	echo "$encrypted_var" >> users.txt

<Kode ini mengambil variabel dari alamatnya dan kirim ke file users.txt dan juga membuat itu sekaligus>

![2cdf](Gambar/2cdf.png)

**Pengerjaan 2d:**

Shinichi juga ingin program register yang ia buat akan memunculkan respon setiap kali ia melakukan register. Respon ini akan menampilkan apakah register yang dilakukan Shinichi berhasil atau gagal

	echo "successfully registered"
		echo $(date '+%Y-%m-%d %H:%M:%S') >> auth.log
		echo "Successfull Registration by $Name" >> auth.log
	else
	    echo -e "This password is not accepted \n"
    	echo $(date '+%Y-%m-%d %H:%M:%S') >> auth.log
    	echo "Failed Registration" >> auth.log
	fi
*Sama seperti kode di C ini langsung membuat file auth.log dan ketika gagal atau sukses akan mengirim tanggal dieksekusi dan sukses tidaknya*

![2cdf](Gambar/2cdf.png)

**Pengerjaan 2e:**

Setelah melakukan register, Shinichi akan langsung melakukan login untuk memastikan bahwa ia telah berhasil membuat akun baru. Login hanya perlu dilakukan menggunakan email dan password.

	echo -e  "Login \n"
	echo -e  "Email"
	read email
	echo -e  "E-Password"
	read password
*Untuk input*

	if grep -q "$email" "users.txt"; then
		if grep -q "$password" "users.txt"; then
*untuk mengecek ada tidaknya email dan password pada users.txt*

![2e](Gambar/2e.png)

**Pengerjaan 2f:**

Ketika login berhasil ataupun gagal, program akan memunculkan respon di mana respon tersebut harus mengandung username dari email yang telah didaftarkan.

Ex: 
LOGIN SUCCESS - Welcome, [username]
LOGIN FAILED - email [email] not registered, please register first

Shinichi juga mencatat seluruh log ke dalam folder users file auth.log, baik login ataupun register.

Format: [date] [type] [message]
Type: REGISTER SUCCESS, REGISTER FAILED, LOGIN SUCCESS, LOGIN FAILED

Ex:
[23/09/17 13:18:02] [REGISTER SUCCESS] user [username] registered successfully
[23/09/17 13:22:41] [LOGIN FAILED] ERROR Failed login attempt on user with email [email]

			echo "LOGIN SUCCESS-Welcome"
			echo $(date '+%Y-%m-%d %H:%M:%S') >> auth.log
	       	echo "Successfull Login by $email" >> auth.log
		else
			F=1
		fi
	else
		F=1
	fi
	if [ $F -eq 1 ]; then
		echo "LOGIN FAILED - email $email not registered,please register first"
	        echo $(date '+%Y-%m-%d %H:%M:%S') >> auth.log
	        echo "Failed Login">> auth.log
	fi
*Mengirim sukses tidaknya login dan waktu melakukan login*

![2cdf](Gambar/2cdf.png)

## Nomor 3
Aether adalah seorang gamer yang sangat menyukai bermain game Genshin Impact. Karena hobinya, dia ingin mengoleksi foto-foto karakter Genshin Impact. Suatu saat Pierro memberikannya sebuah Tautan yang berisi koleksi kumpulan foto karakter dan sebuah clue yang mengarah ke endgame. Ternyata setiap nama file telah dienkripsi dengan menggunakan base64. Karena penasaran dengan apa yang dikatakan piero, Aether tidak menyerah dan mencoba untuk mengembalikan nama file tersebut kembali seperti semula.

Aether membuat script bernama genshin.sh, untuk melakukan unzip terhadap file yang telah diunduh dan decode setiap nama file yang terenkripsi dengan base64 . Karena pada file list_character.csv terdapat data lengkap karakter, Aether ingin merename setiap file berdasarkan file tersebut. Agar semakin rapi, Aether mengumpulkan setiap file ke dalam folder berdasarkan region tiap karakter

Format: Nama - Region - Elemen - Senjata.jpg

Karena tidak mengetahui jumlah pengguna dari tiap senjata yang ada di folder "genshin_character".Aether berniat untuk menghitung serta menampilkan jumlah pengguna untuk setiap senjata yang ada
Format: [Nama Senjata] : [total]

Untuk menghemat penyimpanan. Aether menghapus file - file yang tidak ia gunakan, yaitu genshin_character.zip, list_character.csv, dan genshin.zip

Namun sampai titik ini Aether masih belum menemukan clue endgame yang disinggung oleh Pierro. Dia berfikir keras untuk menemukan pesan tersembunyi tersebut. Aether membuat script baru bernama find_me.sh untuk melakukan pengecekan terhadap setiap file tiap 1 detik. Pengecekan dilakukan dengan cara meng-ekstrak tiap gambar dengan menggunakan command steghide. Dalam setiap gambar tersebut, terdapat sebuah file txt yang berisi string. Aether kemudian mulai melakukan dekripsi dengan base64 pada tiap file txt dan mendapatkan sebuah url. Setelah mendapatkan url yang ia cari, Aether akan langsung menghentikan program find_me.sh serta mendownload file berdasarkan url yang didapatkan.

Dalam prosesnya, setiap kali Aether melakukan ekstraksi dan ternyata hasil ekstraksi bukan yang ia inginkan, maka ia akan langsung menghapus file txt tersebut. Namun, jika itu merupakan file txt yang dicari, maka ia akan menyimpan hasil dekripsi-nya bukan hasil ekstraksi. Selain itu juga, Aether melakukan pencatatan log pada file image.log untuk setiap pengecekan gambar


**Pengerjaan genshin.sh**

	wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2' -O genshin.zip

Ini adalah perintah untuk mengunduh file "genshin.zip" dari URL yang diberikan.

	unzip genshin.zip
	unzip genshin_character.zip

Ini adalah langkah untuk mengunzip file file yang telah diunduh.

	mkdir -p genshin_character

Ini adalah perintah untuk membuat folder bernama "genshin_character".

	declare -A weapon_count

Ini adalah perintah untuk ngedeklarasiin  array bernama "weapon_count" yang nanti digunakan untuk menghitung jumlah pengguna senjata.

	for encrypted_file in *.jpg; do: 

Ini adalah perintah untuk ngeloop.  Loopnya akan mencari semua jpg di directory yang ada sekarang.

	decoded_name=$(echo "$encrypted_file" | base64 -d): 

disini decoced_namenya berfungsi untuk mendeclare variabel decoded_name untuk nyimpen data dari bacaan encrypted file, base64 -d untuk mendekrip nama nama jpg yang ke enkrip sama base 64, kemudian di print menggunakan echo

	mv "$encrypted_file" "genshin_character/${decoded_name}.jpg": 

mv ini adalah perintah buat  (move) yang nanti akan memindahkan jpg yang kebaca di encrypted file, ke dalam folder dan di decode

	echo "File $encrypted_file telah diubah menjadi genshin_character/${decoded_name}.jpg": 

untuk echo ini opsional , hanya supayaa keliatan berhasil berubah atau tidak

	while IFS=, read -r name region element weapon; do: 

Ini adalah loop while yang membaca baris-baris dari file "list_character.csv". Setiap baris dibagi menjadi empat bagian yang dipisahkan oleh tanda koma (,), yaitu name, region, element, dan weapon.

	new_name="${name}-${region}-${element}-${weapon}.jpg"
ini berfungsi untuk ngedeclare untuk fungsi newname yang akan digunakan

	if [ -e "genshin_character/${name}.jpg" ]; then
	mv "genshin_character/${name}.jpg" "genshin_character/${new_name}"
	echo "File genshin_character/${name}.jpg telah diubah menjadi genshin_character/${new_name}"

disini saya menggunakan if untuk memastikan nama file serta memindahkan file yang berasal dari hanya nama dekripsi, berubah menjadi nama yang telah diubah. Serta menunjukkan statu apakah file berhasil diganti nama atau tidak

	if [ -n "${weapon}" ]; then

Kemudian if disini saya buat untuk mengecek weapon

	weapon="$(echo -e "${weapon}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"

ini berfungsi untuk mendeklarasikan weapon untuk menulis semua senjata yang ada, dan untuk sed sendiri cuma buat ngehapus spasi

	weapon_count["${weapon}"]=$((weapon_count["${weapon}"] + 1))
    fi

Kita gunakan array untuk menghitung weaponnya, jika bertemu weaponnya akan di +1

	mkdir -p "genshin_character/${region}

command ini untuk  membuat folder sesuai yang dibaca dari regionnya

	mv "genshin_character/${new_name}" "genshin_character/${region}/${new_name}"
	echo "File genshin_character/${new_name} telah dipindahkan ke genshin_character/${region}/${new_name}"

Command ini digunakan untuk memindahkan file ke dalam masing masing file yang sudah dibuat sesuai regionnya, serta dibawahnya terdapat echo untuk memberikan status keberhasilan pemindahan file.

	else
    echo "File genshin_character/${name}.jpg tidak ditemukan."
    fi

command ini digunakan untuk menunjukkan bahwa cmd tidak menemukan file-file

	done < "list_character.csv"

Ini adalah command yang digunakan untuk memberhentikan loop, serta membuat loop merujuk pada file list_character.csv

	echo "Jumlah pengguna senjata:"
	for weapon in "${!weapon_count[@]}"; do
    echo "${weapon} : ${weapon_count["${weapon}"]}"
	done

 di echo ini dibuat untuk nanpilin jumlah senjatanya

	rm -f genshin_character.zip list_character.csv genshin.zip

rm pada command ini bertujuan untuk menghapus file yang ingin dihapus

**Output genshin.sh**

"Output dari genshin.sh"
![3a1](Gambar/3a1.png)

tampak folder setelah script di jalankan
![3a2](Gambar/3a2.png)

tampak folder lebih dalam setelah script di jalankan
![3a3](Gambar/3a3.png)

**Penjelasan find_me.sh**

	image_dir="."

Baris ini mendefinisikan variabel image_dir yang digunakan untuk menyimpan direktori di mana semua gambar berada.

	download_dir="$image_dir"

ini mendefinisikan variabel download_dir yang digunakan untuk menyimpan direktori di mana file unduhan akan disimpan

	log_file="find_me.log"

ini mendefinisikan variabel log_file yang digunakan untuk menyimpan nama file log yang akan digunakan untuk mencatat pesan log selama eksekusi skrip.

	log_message() {
    local status="$1"
    local image_file="$2"
    local message="$3"
    local timestamp=$(date +"%y/%m/%d %H:%M:%S")
    echo "[$timestamp] [$status] [$image_file] - $message" >> "$log_file"

Ini adalah definisi fungsi log_message() yang akan digunakan untuk mencatat pesan log ke dalam file log yang telah ditentukan

	mkdir -p "$download_dir"

Baris ini menggunakan perintah mkdir untuk membuat direktori unduhan jika belum ada. Opsi -p digunakan agar tidak ada kesalahan jika direktori sudah ada.

	status="NOT FOUND"

Baris ini mendefinisikan variabel status yang akan digunakan untuk menentukan apakah teks telah ditemukan. 

	find "$image_dir" -type f -name "*.jpg" | while read -r image_file; do

Baris ini menggunakan perintah find untuk mencari semua file dengan ekstensi .jpg dalam direktori (termasuk subfolder) yang telah ditentukan oleh image_dir. Hasilnya disaring melalui while read -r untuk membaca setiap baris (nama file) yang ditemukan.

	steghide extract -sf "$image_file" -xf extracted_text.txt -p ""

Baris ini menggunakan perintah steghide untuk mencoba mengekstrak teks dari gambar yang sedang diproses ($image_file) dan menyimpannya dalam file extracted_text.txt. Opsi -p "" digunakan untuk tidak mengatur kata sandi, sehingga ekstraksi hanya dilakukan tanpa kata sandi.

	if [ $? -eq 0 ]; then

Baris ini memeriksa apakah perintah steghide sebelumnya berhasil dieksekusi (kodenya 0), menunjukkan bahwa ekstraksi berhasil.

	extracted_text=$(cat extracted_text.txt | base64 -d)

Baris ini membaca teks yang diekstrak dari file extracted_text.txt dan kemudian mendekode teks tersebut dengan Base64

	log_message "INFO" "$image_file" "Extracted Text: $extracted_text"

Baris ini memanggil fungsi log_message() untuk mencatat pesan log dengan status "INFO" yang berisi teks yang telah diekstrak dari gambar.

	if [[ $extracted_text == *"http"* ]]; then

Baris ini memeriksa apakah teks yang diekstrak mengandung URL (http).

	url_filename=$(basename "$extracted_text")

Baris ini digunakan untuk mendapatkan nama file dari URL dengan menggunakan perintah basename

	wget "$extracted_text" -P "$download_dir"

Baris ini menggunakan perintah wget untuk mengunduh file yang terdapat dalam URL yang ditemukan dan menyimpannya dalam direktori unduhan yang telah ditentukan ($download_dir).

	log_message "FOUND" "$image_file" "URL diunduh: $extracted_text"

Baris ini memanggil fungsi log_message() untuk mencatat pesan log dengan status "FOUND" yang berisi URL yang telah diunduh.

	jpg_filename=$(basename "$image_file")
	text_filename="${jpg_filename%.jpg}.txt"

Baris ini digunakan untuk mendapatkan nama file gambar (dengan ekstensi .jpg) yang sedang diproses dan kemudian menghasilkan nama file teks dengan mengganti ekstensi .jpg menjadi .txt.

	echo "$extracted_text" > "$text_filename"

Baris ini menyimpan hasil dekripsi teks dalam file teks baru yang telah dibuat.

	rm -f extracted_text.txt

Baris ini menghapus file teks hasil ekstraksi (extracted_text.txt) setelah selesai menggunakannya.

	break

Baris ini digunakan untuk keluar dari loop saat URL ditemukan, sehingga skrip akan berhenti saat URL ditemukan

	else

Baris ini menangani kasus ketika URL tidak ditemukan dalam teks yang diekstrak.

	log_message "NOT FOUND" "$image_file" "Tidak ada URL yang ditemukan."

Baris ini memanggil fungsi log_message() untuk mencatat pesan log dengan status "NOT FOUND" yang mengindikasikan bahwa tidak ada URL yang ditemukan dalam teks.

	fi

Baris ini menutup blok if yang memeriksa URL.

**Output find_me.sh**

tampak terminal setelah find_me.sh berjalan
![3b1](Gambar/3b1.png)

Tampak folder setelah find_me.sh
![3b2](Gambar/3b2.png)

log file find_me.sh
![3b3](Gambar/3b3.png)

Berikut adalah Link yang didapatkan setelah di decrypt
![3b4](Gambar/3b4.png)


## Nomor 4
Defatra sangat tergila-gila dengan laptop legion nya. Suatu hari, laptop legion nya mendadak rusak 🙁 Tentu saja, Defatra adalah programer yang harus 24/7 ngoding tanpa menggunakan vscode di laptop legion nya.  Akhirnya, dia membawa laptop legion nya ke tukang servis untuk diperbaiki. Setelah selesai diperbaiki, ternyata biaya perbaikan sangat mahal sehingga dia harus menggunakan uang hasil slot nya untuk membayarnya. Menurut Kang Servis, masalahnya adalah pada laptop Defatra yang overload sehingga mengakibatkan crash pada laptop legion nya. Untuk mencegah masalah serupa di masa depan, Defatra meminta kamu untuk membuat program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada laptop kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/. 

a.	Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash 

b.	Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit. 

c.	Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2023013115.log dengan format metrics_agg_{YmdH}.log 

d.	Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file


**Pengerjaan 4a:**

Pada soal ini, kita diminta untuk membuat sebuah monitoring resource yang memonitor ram, disk, dan swap pada suatu target directory yaitu /home/{user}/

	user=$(whoami)

Ini adalah perintah untuk menentukan nama pengguna yang sedang menjalankan script.

	target_path="/home/$user/"

Ini adalah variabel yang menyimpan path ke direktori beranda pengguna yang sedang menjalankan script.

	mem_info=($(free -m | awk 'NR==2{print $2,$3,$4,$5,$6,$7}')):

Ini adalah perintah yang mengambil informasi tentang penggunaan memori dari perintah ‘free -m’, lalu menyimpannya dalam sebuah array bernama ‘mem_info’. ‘Awk NR==2’ ini mencakup total memori, memori yang digunakan, memori yang tersedia, dan lainnya.

	mem_total="${mem_info[0]}"
	mem_used="${mem_info[1]}"
	mem_free="${mem_info[2]}"
	mem_shared="${mem_info[3]}"
	mem_buff="${mem_info[4]}"
	mem_available="${mem_info[5]}"

Perintah diatas digunakan untuk menyimpan variabel yang telah diambil dari awk

	swap_info=($(free -m | awk 'NR==3{print $2,$3,$4}')):

Ini adalah perintah yang mengambil informasi tentang penggunaan swap dari perintah free -m, lalu menyimpannya dalam sebuah array bernama swap_info. awk NR==3 ini mencakup total swap, swap yang digunakan, dan swap yang tersedia.

	swap_total="${swap_info[0]}"
	swap_used="${swap_info[1]}"
	swap_free="${swap_info[2]}"

Perintah diatas digunakan untuk menyimpan variabel yang telah diambil dari awk.

	path_size=$(du -sh "$target_path" | awk '{print $1}')

Ini adalah perintah untuk menghitung ukuran total direktori beranda pengguna yang sedang menjalankan script dan menyimpannya dalam variabel ‘path_size’

	timestamp=$(date +'%Y%m%d%H%M%S'):

Ini adalah perintah yang menghasilkan timestamp dalam format YYYYMMDDHHMMSS dan menyimpannya dalam variabel timestamp

	log_directory="/home/$user/log":

Ini adalah variabel yang menyimpan path ke direktori log, yang akan digunakan untuk menyimpan file log.

	log_filename="metrics_$timestamp.log":

Ini adalah variabel yang menyimpan nama file log dengan format metrics_TIMESTAMP.log.

	log_file_path="$log_directory/$log_filename":

Ini adalah variabel yang menyimpan path lengkap ke file log yang akan dibuat.

	mkdir -p "$log_directory":

Ini adalah perintah untuk membuat direktori log jika belum ada

	echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > "$log_file_path": 

Ini adalah perintah untuk menulis header kolom ke file log yang baru dibuat.

	echo "$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$target_path,$path_size" >> "$log_file_path": 

Ini adalah perintah untuk menulis data penggunaan sumber daya sistem ke file log.

	chmod 600 "$log_file_path": 

Ini adalah perintah untuk mengatur izin file log sehingga hanya pemiliknya yang dapat membacanya.

	echo "Hasil telah disimpan dalam file log: $log_file_path": 

Ini adalah perintah untuk mencetak pesan ke layar yang memberitahu pengguna bahwa hasil telah disimpan dalam file log.

**Pengerjaan 4b:**

Supaya Script dapat berjalan otomatis setiap menit maka kita perlu menambahkan

	* * * * * /home/dori/soal4/minute_log.sh


**Pengerjaan 4c:**

Pada bagian 4c, scriptnya memiliki beberapa bagian yang mirip, namun terdapat beberapa tambahan nilai maximum, minimum ,serta average dari metrics yang terdapat di 4a, kemudian juga script berubah menjadi tergenerate setiap jam, maka langkahnya :

	0 * * * * /home/dori/soal4/aggregate_minutes_to_hourly_log.sh

ini adalah text yang harus dimasukkan dalam crontab guna memembuat file tergenerate per jam

	user=$(whoami)

Ini adalah perintah yang akan digunakan untuk menentukan nama pengguna yang sedang menjalankan script.

	target_path="/home/$user/"

Ini adalah variabel yang menyimpan path ke direktori beranda pengguna yang sedang menjalankan script.

	timestamp=$(date +'%Y%m%d%H%M%S')

Ini adalah perintah yang menghasilkan timestamp dalam format YYYYMMDDHHMMSS dan menyimpannya dalam variabel timestamp, untuk penamaan file nantinya.

	log_directory="/home/$user/log"

Ini adalah variabel yang menyimpan path ke direktori log, yang akan digunakan untuk menyimpan file log.

	log_filename="metrics_$timestamp.log"

Ini adalah variabel yang menyimpan nama file log dengan format metrics_TIMESTAMP.log.

	log_file_path="$log_directory/$log_filename":

Ini adalah variabel yang menyimpan path lengkap ke file log yang akan dibuat.

	mkdir -p "$log_directory":

Ini adalah perintah untuk membuat direktori log jika belum ada

	mem_info=($(free -m | awk 'NR==2{print $2,$3,$4,$5,$6,$7}')):

Ini adalah perintah yang mengambil informasi tentang penggunaan memori dari perintah ‘free -m’, lalu menyimpannya dalam sebuah array bernama ‘mem_info’. ‘Awk NR==2’ ini mencakup total memori, memori yang digunakan, memori yang tersedia, dan lainnya

	mem_total="${mem_info[0]}"
	mem_used="${mem_info[1]}"
	mem_free="${mem_info[2]}"
	mem_shared="${mem_info[3]}"
	mem_buff="${mem_info[4]}"
	mem_available="${mem_info[5]}"

Perintah diatas digunakan untuk menyimpan variabel yang telah diambil dari awk

	swap_info=($(free -m | awk 'NR==3{print $2,$3,$4}')):

Ini adalah perintah yang mengambil informasi tentang penggunaan swap dari perintah free -m, lalu menyimpannya dalam sebuah array bernama swap_info. awk NR==3 ini mencakup total swap, swap yang digunakan, dan swap yang tersedia.

	swap_total="${swap_info[0]}"
	swap_used="${swap_info[1]}"
	swap_free="${swap_info[2]}"

Perintah diatas digunakan untuk menyimpan variabel yang telah diambil dari awk

	path_size=$(du -sh "$target_path" | awk '{print $1}')

Ini adalah perintah untuk menghitung ukuran total direktori beranda pengguna yang sedang menjalankan script dan menyimpannya dalam variabel path_size

	ram_metrics=("$mem_total" "$mem_used" "$mem_free" "$mem_shared" "$mem_buff" "$mem_available")

Ini adalah array yang berisi informasi penggunaan memori fisik.

	ram_min=$(printf "%s\n" "${ram_metrics[@]}" | sort -n | head -n 1)

Ini adalah perintah untuk menemukan nilai minimum dari array ram_metrics.

	ram_max=$(printf "%s\n" "${ram_metrics[@]}" | sort -n | tail -n 1)

 Ini adalah perintah untuk menemukan nilai maksimum dari array ram_metrics.

	ram_sum=0

Ini adalah variabel yang digunakan untuk menghitung jumlah semua nilai dalam array ram_metrics.

	for metric in "${ram_metrics[@]}"; do...done

Ini adalah loop yang menghitung jumlah semua nilai dalam array ram_metrics dan menyimpannya dalam variabel ram_sum.

	ram_avg=$(bc -l <<< "scale=1; $ram_sum / ${#ram_metrics[@]}")

Ini adalah perintah yang menghitung rata-rata nilai dalam array ram_metrics dengan menggunakan perhitungan dalam presisi desimal dan menyimpannya dalam variabel ram_avg

	echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

Ini adalah perintah untuk menulis header kolom ke  layar

	echo "minimum,$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$target_path,$path_size,$ram_min"
	echo "maximum,$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$target_path,$path_size,$ram_max"
	echo "average,$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$target_path,$path_size,$ram_avg"

Ini adalah script untuk mencetak data agregasi ke layar dalam 3 baris terpisah

	echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
	echo "minimum,$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$target_path,$path_size,$ram_min"
	echo "maximum,$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$target_path,$path_size,$ram_max"
	echo "average,$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$target_path,$path_size,$ram_avg"
} 

Pada echo ini, script mengalihkan (redirect) output ke file log yang telah dibuat

	echo "Metrics telah disimpan dalam file log: $log_file_path"

script ini berfungsi untuk memberitahukan status file log


**Pengerjaan 4d:**

Karena kita ingin file log hanya dapat dibaca oleh user pemilik file. maka kita harus menambahkan 

	chmod 600 "$log_file_path"

**Output minute_log.sh**

Hasil minute_log

![Hasil minute_log](Gambar/4a1.png)

log hasil

![log minute_log](Gambar/4a2.png)

**Output aggregate_minutes_to_hourly_log.sh**

Hasil aggregate_minutes_to_hourly_log

![Hasil agg](Gambar/4b1.png)

log aggregate_minutes_to_hourly_log

![log agg](Gambar/4b2.png)

Terimakasih Telah membaca
