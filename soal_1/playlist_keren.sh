#!/bin/bash
cat playlist.csv
grep "hip hop" playlist.csv > hiphop.csv
sort -t "," -k 15,15nr hiphop.csv > sorted_hiphop.csv
head -n 5 sorted_hiphop.csv

grep "John Mayer" playlist.csv > johnmayer.csv
sort -t "," -k 15,15 johnmayer.csv > sorted_johnmayer.csv
head -n 5 sorted_johnmayer.csv

grep "2004" playlist.csv > 2004.csv
sort -t "," -k 15,15nr 2004.csv > sorted_2004.csv
head -n 10 sorted_2004.csv

grep "Sri" playlist.csv
