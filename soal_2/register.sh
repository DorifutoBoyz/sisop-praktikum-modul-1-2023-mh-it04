#!/bin/bash
echo -e  "Register \n"
echo -e  "Name "
read Name
echo -e  "Email"
read email
echo -e  "Password"
read password
n=${#password}
b=8
l=0
C=0
nC=0
nS=0
N=0
regex="[[:punct:]]"
UE=0
F=0
o=1
if [ -z "$Name" ]; then
        echo -e "Insert a name"
	F=1
fi
if [ -z "$email" ]; then
        echo -e "Insert an email"
        F=1
fi
if [[ $email =~ ['!@#$%^&*()-_+=|\:;<,>.?/'] ]]; then
        C=0
else
	echo "Proper email address required"
	F=1
fi
if [ $n  -lt $b ];
 then
	echo -e "Less than 8 \n"
 fi
if [[ $C =~  [A-Z] ]]; then
        echo -e "No capital word \n"
        F=1
fi
if [[ $nC =~ [a-z] ]]; then
	echo -e "No non-capital   word \n"
	F=1
fi
if [[ "$password" == "$Name" ]]; then
	echo -e "Passsword and name is the same \n"
	F=1
fi
if [[ $password =~ [0-9] ]]; then
	N=1
else
	echo -e "No number \n"
	F=1
fi
if [[ $password =~ ['!@#$%^&*()-_+=|\:;<,>.?/'] ]]; then
	UE=1
else
	echo -e "No unique symbol"
	F=1
fi
if [ $F -eq  0 ]; then
	echo   "This password is good "
	echo  "Encrypting password"
	encrypted_var=$(echo -n "$password" | openssl enc -e -aes-256-cbc -a -salt -pass pass:rizki4132)
	echo "$Name" >> users.txt
        echo "$email" >> users.txt
	echo "$encrypted_var" >> users.txt
	echo "Save this"
	echo "$encrypted_var"
	echo "successfully registered"
	echo $(date '+%Y-%m-%d %H:%M:%S') >> auth.log
	echo "Successfull Registration by $Name" >> auth.log
else
        echo -e "This password is not accepted \n"
        echo $(date '+%Y-%m-%d %H:%M:%S') >> auth.log
        echo "Failed Registration" >> auth.log
fi
