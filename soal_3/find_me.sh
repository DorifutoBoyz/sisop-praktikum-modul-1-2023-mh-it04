#!/bin/bash

image_dir="."

download_dir="$image_dir"

log_file="find_me.log"

log_message() {
    local status="$1"
    local image_file="$2"
    local message="$3"
    local timestamp=$(date +"%y/%m/%d %H:%M:%S")
    echo "[$timestamp] [$status] [$image_file] - $message" >> "$log_file"
}

mkdir -p "$download_dir"

status="NOT FOUND"

find "$image_dir" -type f -name "*.jpg" | while read -r image_file; do
    steghide extract -sf "$image_file" -xf extracted_text.txt -p ""

    if [ $? -eq 0 ]; then
        extracted_text=$(cat extracted_text.txt | base64 -d)

        log_message "INFO" "$image_file" "Extracted Text: $extracted_text"
        
        if [[ $extracted_text == *"http"* ]]; then
            echo "URL Found: $extracted_text"
            
            url_filename=$(basename "$extracted_text")

            wget "$extracted_text" -P "$download_dir"
            log_message "FOUND" "$image_file" "URL diunduh: $extracted_text"

            status="FOUND"

            jpg_filename=$(basename "$image_file")
            text_filename="${jpg_filename%.jpg}.txt"
            echo "$extracted_text" > "$text_filename"
            rm -f extracted_text.txt

            break
        else
            log_message "NOT FOUND" "$image_file" "Tidak ada URL yang ditemukan."
        fi

        rm -f extracted_text.txt
    else
        log_message "ERROR" "$image_file" "Ekstraksi gagal"
    fi

    sleep 1
done

if [ "$status" = "NOT FOUND" ]; then
    log_message "INFO" "" "Tidak ada teks yang ditemukan."
fi

