#!/bin/bash

wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2' -O genshin.zip

unzip genshin.zip

unzip genshin_character.zip

mkdir -p genshin_character

declare -A weapon_count

for encrypted_file in *.jpg; do
    decoded_name=$(echo "$encrypted_file" | base64 -d)

    mv "$encrypted_file" "genshin_character/${decoded_name}.jpg"

    echo "File $encrypted_file telah diubah menjadi genshin_character/${decoded_name}.jpg"
done

while IFS=, read -r name region element weapon; do
    new_name="${name}-${region}-${element}-${weapon}.jpg"

    if [ -e "genshin_character/${name}.jpg" ]; then
        mv "genshin_character/${name}.jpg" "genshin_character/${new_name}"
        echo "File genshin_character/${name}.jpg telah diubah menjadi genshin_character/${new_name}"

        if [ -n "${weapon}" ]; then
            weapon="$(echo -e "${weapon}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
            weapon_count["${weapon}"]=$((weapon_count["${weapon}"] + 1))
        fi

        mkdir -p "genshin_character/${region}"

        mv "genshin_character/${new_name}" "genshin_character/${region}/${new_name}"
        echo "File genshin_character/${new_name} telah dipindahkan ke genshin_character/${region}/${new_name}"
    else
        echo "File genshin_character/${name}.jpg tidak ditemukan."
    fi
done < "list_character.csv"

echo "Jumlah pengguna senjata:"
for weapon in "${!weapon_count[@]}"; do
    echo "${weapon} : ${weapon_count["${weapon}"]}"
done

rm -f genshin_character.zip list_character.csv genshin.zip
