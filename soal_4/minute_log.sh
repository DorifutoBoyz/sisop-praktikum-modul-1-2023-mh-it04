#!/bin/bash

#Setting Crontab
#* * * * * /home/dori/soal4/minute_log.sh

user=$(whoami)

target_path="/home/$user/"

mem_info=($(free -m | awk 'NR==2{print $2,$3,$4,$5,$6,$7}'))
mem_total="${mem_info[0]}"
mem_used="${mem_info[1]}"
mem_free="${mem_info[2]}"
mem_shared="${mem_info[3]}"
mem_buff="${mem_info[4]}"
mem_available="${mem_info[5]}"

swap_info=($(free -m | awk 'NR==3{print $2,$3,$4}'))
swap_total="${swap_info[0]}"
swap_used="${swap_info[1]}"
swap_free="${swap_info[2]}"

path_size=$(du -sh "$target_path" | awk '{print $1}')

timestamp=$(date +'%Y%m%d%H%M%S')

log_directory="/home/$user/log"
log_filename="metrics_$timestamp.log"
log_file_path="$log_directory/$log_filename"

mkdir -p "$log_directory"

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > "$log_file_path"
echo "$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$target_path,$path_size" >> "$log_file_path"
chmod 600 "$log_file_path"

echo "Hasil telah disimpan dalam file log: $log_file_path"

